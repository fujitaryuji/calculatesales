package jp.alhinc.fujita_ryuji.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {
		
		HashMap<String,String> branchName = new HashMap<String,String>(); //店舗コード,支店名
		HashMap<String,Long> storeSales = new HashMap<String,Long>(); //店舗コード,売り上げ long=Max_Value2x63-1
		BufferedReader br = null;
	//1,支店定義ファイルの読み込み
		try {
			File file = new File(args[0],"branch.lst"); //店舗リスト
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			
			String line;
			while((line = br.readLine()) != null) {
				for(int i = 0; i < 2; i++) {
					String[] box = line.split(",");
					branchName.put(box[0], box[1]); //店舗コード(),支店名()
					storeSales.put(box[0], (long)0); //店舗コード(),売り上げ()
					}
				System.out.println(line);
			}
		}catch(IOException e) {
			System.out.println("支店定義ファイルが存在しません");
		}finally {
			if(br != null) {
				try {
					br.close();
				}catch(IOException e) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
				}
			}
		}
		//2.売り上げファイルの読み込み
		try {
			File f = new File(args[0]);
			File[] list = f.listFiles();
			for(int i = 0; i < list.length; i++) {
				if(list[i].getName().matches("[0-9]{8}.rcd")) { //rcdの読み込みが8桁の場合
					BufferedReader b = new BufferedReader(new FileReader(list[i]));
					String box = b.readLine();
					Long box2 = Long.valueOf(b.readLine());
					storeSales.put(box, box2);
					System.out.println(storeSales);
					b.close();
				}else{
					System.out.println("");
					   }
				}
			}catch(IOException e) {
				System.out.println("");
			}finally {
				if(br != null) {
					try {
						br.close();
						System.out.println("");
					}catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました。");
					}	
				}
		    }
		//集計
		try {
			File outFile = new File(args[0],"branch.out");
			BufferedWriter bw = new BufferedWriter(new FileWriter(outFile));
			
			for(Map.Entry<String,String> entry : branchName.entrySet()) {
				bw.write(entry.getKey() + "," + entry.getValue() + "," + storeSales.get(entry.getKey()));
				bw.newLine();
			}
			bw.close();
		}catch(IOException e) {
			e.printStackTrace();
			System.out.println("");
		}finally {
			if(br != null) {
				try {
				br.close();
				}catch(IOException e) {
					System.out.println("");
				}
			}
		}
	}	
}		
		
		